import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService  implements InMemoryDbService {
  
  createDb() {
    const players = [
      {name : "Charles", life : 70, maxLife: 70, strength: 10, defense : 3, endurance: 10, agility: 10, items: [], bonus: [], attacks: [
        {name: 'Quick Attack', description: 'A quick attack that gives you priority !', minAttack: 4, maxAttack: 6, reloadTime: 1, counter: 1, type: "Attack"},
        {name: 'Base Attack', description: 'A normal attack ! Nothing special !', minAttack: 5, maxAttack: 7, reloadTime: 1, counter: 1, type: "Attack"},
        {name: 'Heavy Attack', description: 'Heavy damage ! But you will need some rest before do it again !', minAttack: 6, maxAttack: 10, reloadTime : 2, counter: 1, type: "Attack"},
      ]},
      {name : "Olivier", life : 70, maxLife: 70, strength: 10, defense : 3, endurance: 10, agility: 10, items: [], bonus: [], attacks: [
        {name: 'Quick Attack', description: 'A quick attack that gives you priority !', minAttack: 4, maxAttack: 6, reloadTime: 1, counter: 1, type: "Attack"},
        {name: 'Base Attack', description: 'A normal attack ! Nothing special !', minAttack: 5, maxAttack: 7, reloadTime: 1, counter: 1, type: "Attack"},
        {name: 'Heavy Attack', description: 'Heavy damage ! But you will need some rest before do it again !', minAttack: 6, maxAttack: 10, reloadTime : 2, counter: 1, type: "Attack"},
      ]},
      {name : "Benjamin", life : 70, maxLife: 70, strength: 10, defense : 3, endurance: 10, agility: 10, items: [], bonus: [], attacks: [
        {name: 'Quick Attack', description: 'A quick attack that gives you priority !', minAttack: 4, maxAttack: 6, reloadTime: 1, counter: 1, type: "Attack"},
        {name: 'Base Attack', description: 'A normal attack ! Nothing special !', minAttack: 5, maxAttack: 7, reloadTime: 1, counter: 1, type: "Attack"},
        {name: 'Heavy Attack', description: 'Heavy damage ! But you will need some rest before do it again !', minAttack: 6, maxAttack: 10, reloadTime : 2, counter: 1, type: "Attack"},
      ]},
      {name : "Ludovic", life : 70, maxLife: 70, strength: 10, defense : 3, endurance: 10, agility: 10, items: [], bonus: [], attacks: [
        {name: 'Quick Attack', description: 'A quick attack that gives you priority !', minAttack: 4, maxAttack: 6, reloadTime: 1, counter: 1, type: "Attack"},
        {name: 'Base Attack', description: 'A normal attack ! Nothing special !', minAttack: 5, maxAttack: 7, reloadTime: 1, counter: 1, type: "Attack"},
        {name: 'Heavy Attack', description: 'Heavy damage ! But you will need some rest before do it again !', minAttack: 6, maxAttack: 10, reloadTime : 2, counter: 1, type: "Attack"},
      ]},
      {name : "Romain", life : 70, maxLife: 70, strength: 10, defense : 3, endurance: 10, agility: 10, items: [], bonus: [], attacks: [
        {name: 'Quick Attack', description: 'A quick attack that gives you priority !', minAttack: 4, maxAttack: 6, reloadTime: 1, counter: 1, type: "Attack"},
        {name: 'Base Attack', description: 'A normal attack ! Nothing special !', minAttack: 5, maxAttack: 7, reloadTime: 1, counter: 1, type: "Attack"},
        {name: 'Heavy Attack', description: 'Heavy damage ! But you will need some rest before do it again !', minAttack: 6, maxAttack: 10, reloadTime : 2, counter: 1, type: "Attack"},
      ]},
      {name : "Quentin", life : 70, maxLife: 70, strength: 10, defense : 3, endurance: 10, agility: 10, items: [], bonus: [], attacks: [
        {name: 'Quick Attack', description: 'A quick attack that gives you priority !', minAttack: 4, maxAttack: 6, reloadTime: 1, counter: 1, type: "Attack"},
        {name: 'Base Attack', description: 'A normal attack ! Nothing special !', minAttack: 5, maxAttack: 7, reloadTime: 1, counter: 1, type: "Attack"},
        {name: 'Heavy Attack', description: 'Heavy damage ! But you will need some rest before do it again !', minAttack: 6, maxAttack: 10, reloadTime : 2, counter: 1, type: "Attack"},
      ]}, 
      {name : "Maxime", life : 70, maxLife: 70, strength: 10, defense : 3, endurance: 10, agility: 10, items: [], bonus: [], attacks: [
        {name: 'Quick Attack', description: 'A quick attack that gives you priority !', minAttack: 4, maxAttack: 6, reloadTime: 1, counter: 1, type: "Attack"},
        {name: 'Base Attack', description: 'A normal attack ! Nothing special !', minAttack: 5, maxAttack: 7, reloadTime: 1, counter: 1, type: "Attack"},
        {name: 'Heavy Attack', description: 'Heavy damage ! But you will need some rest before do it again !', minAttack: 6, maxAttack: 10, reloadTime : 2, counter: 1, type: "Attack"},
      ]},
      {name : "Nicolas", life : 70, maxLife: 70, strength: 10, defense : 3, endurance: 10, agility: 10, items: [], bonus: [], attacks: [
        {name: 'Quick Attack', description: 'A quick attack that gives you priority !', minAttack: 4, maxAttack: 6, reloadTime: 1, counter: 1, type: "Attack"},
        {name: 'Base Attack', description: 'A normal attack ! Nothing special !', minAttack: 5, maxAttack: 7, reloadTime: 1, counter: 1, type: "Attack"},
        {name: 'Heavy Attack', description: 'Heavy damage ! But you will need some rest before do it again !', minAttack: 6, maxAttack: 10, reloadTime : 2, counter: 1, type: "Attack"},
      ]}
    ];
    return {players};
  }
}