import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Create a route using lazy loading
//and create a default route redirecting to the module fight using '**'
const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
