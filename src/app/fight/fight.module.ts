import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FightRoutingModule } from './fight-routing.module';
import { SharedModule } from '../_utils/shared.module';
import { PlayerSelectionComponent } from './_components/player-selection/player-selection.component';


@NgModule({
  declarations: [PlayerSelectionComponent],
  imports: [
    CommonModule,
    FightRoutingModule,
    SharedModule
  ]
})
export class FightModule { }
