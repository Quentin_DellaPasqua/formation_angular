import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
  declarations: [
  ],
  exports: [
      MatButtonModule,
      MatCardModule,
      MatProgressBarModule,
      MatIconModule
  ],
  providers: []
})
export class SharedModule { }
