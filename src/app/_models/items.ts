import { Effects } from './effects.enum';
import { Action } from './action';

export class Items extends Action {

    effect: Effects;
    value: number;
    stock: number;

}