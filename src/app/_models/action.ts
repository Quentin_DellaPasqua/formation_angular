export abstract class Action{

    name: string;
    description: string;
    euristicValue?: number;
    baseEurisitic?: number;
    type: string;
}