import { Bonus } from "./bonus";
import { Items } from "./items";
import { Attack } from "./attack";

export class Player {

    name: string;
    strength: number;
    agility: number;
    defense: number;
    endurance: number;
    life: number;
    maxLife: number;
    items: Items[];
    bonus: Bonus[];
    attacks: Attack[];

}
