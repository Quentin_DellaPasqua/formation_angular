import { AttackEffects } from './attack-effects.enum';
import { Action } from './action';

export class Attack extends Action {

    minAttack: number;
    maxAttack: number;
    reloadTime: number;
    counter: number;
    attackEffects?: AttackEffects;
}